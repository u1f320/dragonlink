package views

import (
	"embed"
	"io/fs"
)

//go:embed _layout.html
var Layout string

//go:embed html/*.html
var data embed.FS

var Data fs.FS

func init() {
	fsys, err := fs.Sub(data, "html")
	if err != nil {
		panic(err)
	}
	Data = fsys
}
