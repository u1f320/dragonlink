package db

import (
	"context"

	"codeberg.org/u1f320/dragonlink/ent"
	"emperror.dev/errors"
	_ "github.com/jackc/pgx/v4/stdlib"
)

type DB struct {
	Client   *ent.Client
	Verifier *Verifier
}

func New(dsn, hmacKey string) (db *DB, err error) {
	db = &DB{}
	db.Client, err = ent.Open("pgx", dsn)
	if err != nil {
		return nil, errors.Wrap(err, "creating client")
	}

	db.Verifier, err = NewVerifier(hmacKey)
	if err != nil {
		return nil, errors.Wrap(err, "creating verifier")
	}

	if err := db.Client.Schema.Create(context.Background()); err != nil {
		return nil, errors.Wrap(err, "creating schema")
	}

	return db, nil
}

type ctxKey int

const ctxKeyUser ctxKey = 1

func ContextWithUser(ctx context.Context, u *ent.User) context.Context {
	return context.WithValue(ctx, ctxKeyUser, u)
}

func UserFromContext(ctx context.Context) (u *ent.User, ok bool) {
	v := ctx.Value(ctxKeyUser)
	if v == nil {
		return nil, false
	}

	u, ok = v.(*ent.User)
	return u, ok
}
