package render

import (
	"bytes"
	"html/template"
	"io"
)

// Renderer is a specialized template renderer to handle having a common layout.
type Renderer struct {
	layout         string
	innerTemplates *template.Template
	templateFunc   func(data RenderData) (*template.Template, error)
}

func NewRenderer(layout string, tmpls *template.Template) (*Renderer, error) {
	r := &Renderer{layout: layout, innerTemplates: tmpls}

	_, err := template.New("").Funcs(r.funcs("hi", tmpls, RenderData{})).Parse(layout)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func NewRendererFunc(layout string, tmplFunc func(data RenderData) (*template.Template, error)) (*Renderer, error) {
	_, err := template.New("").Parse(layout)
	if err != nil {
		return nil, err
	}
	r := &Renderer{layout: layout, templateFunc: tmplFunc}
	return r, nil
}

func (r *Renderer) Render(w io.Writer, tmplName string, data RenderData) error {
	inner, err := r.tmpls(data)
	if err != nil {
		return err
	}
	tmpl, err := template.New("").Funcs(r.funcs(tmplName, inner, data)).Parse(r.layout)
	if err != nil {
		return err
	}
	return tmpl.Execute(w, data)
}

func (r *Renderer) tmpls(data RenderData) (*template.Template, error) {
	if r.innerTemplates != nil {
		return r.innerTemplates, nil
	}
	return r.templateFunc(data)
}

func (r *Renderer) funcs(tmplName string, templates *template.Template, data RenderData) template.FuncMap {
	return template.FuncMap{
		"inner": func() template.HTML {
			w := new(bytes.Buffer)
			err := templates.ExecuteTemplate(w, tmplName, data.Inner)
			if err != nil {
				return ""
			}
			return template.HTML(w.String())
		},
	}
}
