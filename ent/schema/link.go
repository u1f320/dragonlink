package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Link holds the schema definition for the Link entity.
type Link struct {
	ent.Schema
}

// Mixins of the Link.
func (Link) Mixin() []ent.Mixin {
	return []ent.Mixin{
		CommonMixin{},
	}
}

// Fields of the Link.
func (Link) Fields() []ent.Field {
	return []ent.Field{
		field.String("url").NotEmpty(),
		field.String("title").NotEmpty(),
		field.String("description").Optional().Nillable(),
	}
}

// Edges of the Link.
func (Link) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).Ref("links").Unique(),
		edge.From("tags", Tag.Type).Ref("links"),
	}
}
