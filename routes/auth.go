package routes

import (
	"codeberg.org/u1f320/dragonlink/db"
	"github.com/go-chi/chi/v5"
)

type Server struct {
	DB *db.DB
}

func Mount(r chi.Router, db *db.DB) {
	s := &Server{DB: db}
	_ = s

	r.Route("/", func(r chi.Router) {
		r.Get("/sign_in", s.getSignIn)
		r.Post("/sign_in", s.postSignIn)

		r.Get("/sign_up", s.getSignUp)
		r.Post("/sign_up", s.postSignUp)
	})
}
