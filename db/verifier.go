package db

import (
	"encoding/base64"
	"fmt"
	"time"

	"emperror.dev/errors"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

type Claims struct {
	UserID uuid.UUID `json:"sub"`
	jwt.RegisteredClaims
}

// Verifier is a struct to create and verify JWTs.
type Verifier struct {
	key []byte
}

// NewVerifier creates a new Verifier
func NewVerifier(hmacKey string) (*Verifier, error) {
	key, err := base64.URLEncoding.DecodeString(hmacKey)
	if err != nil {
		return nil, err
	}

	return &Verifier{key: key}, nil
}

const tokenExpireDays = 30

// CreateToken creates a token for the given user ID.
// It expires after 30 days.
func (v *Verifier) CreateToken(userID uuid.UUID) (string, error) {
	now := time.Now()
	expires := now.Add(tokenExpireDays * 24 * time.Hour)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, Claims{
		UserID: userID,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "pronouns",
			ExpiresAt: jwt.NewNumericDate(expires),
			IssuedAt:  jwt.NewNumericDate(now),
			NotBefore: jwt.NewNumericDate(now),
		},
	})

	return token.SignedString(v.key)
}

// Claims parses the given token and returns its Claims.
// If the token is invalid, returns an error.
func (v *Verifier) Claims(token string) (c Claims, err error) {
	parsed, err := jwt.ParseWithClaims(token, &Claims{}, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf(`unexpected signing method "%v"`, t.Header["alg"])
		}
		return v.key, nil
	})
	if err != nil {
		return c, errors.Wrap(err, "parsing token")
	}

	if c, ok := parsed.Claims.(*Claims); ok && parsed.Valid {
		return *c, nil
	}

	return c, fmt.Errorf("unknown claims type %T", parsed.Claims)
}
