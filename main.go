package main

import (
	"net/http"

	"codeberg.org/u1f320/dragonlink/router"
)

func main() {
	r := router.New(nil)

	http.ListenAndServe(":8080", r.R)
}
