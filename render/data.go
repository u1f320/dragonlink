package render

import "codeberg.org/u1f320/dragonlink/ent"

type RenderData struct {
	User  *ent.User
	Title string
	Inner any
}
