package router

import (
	"html/template"
	"net/http"

	"codeberg.org/u1f320/dragonlink/db"
	"codeberg.org/u1f320/dragonlink/log"
	"codeberg.org/u1f320/dragonlink/render"
	"codeberg.org/u1f320/dragonlink/static"
	"codeberg.org/u1f320/dragonlink/views"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Router struct {
	R chi.Router
}

func New(db *db.DB) (r *Router) {
	r = &Router{R: chi.NewMux()}

	r.R.Use(middleware.Logger)
	r.R.Use(middleware.Recoverer)

	// serve static files on /app/static
	r.R.Handle("/app/static/*", http.StripPrefix("/app/static", http.FileServer(http.FS(static.Data))))

	// create renderer
	tmpl, err := template.New("").ParseFS(views.Data, "*.html")
	if err != nil {
		log.Fatal("parsing templates:", err)
	}

	rd, err := render.NewRenderer(views.Layout, tmpl)
	if err != nil {
		log.Fatal("creating renderer:", err)
	}
	_ = rd

	r.R.Route("/app", func(r chi.Router) {
		
	})

	r.R.Get("/app", func(w http.ResponseWriter, r *http.Request) {
		rd.Render(w, "test.html", render.RenderData{})
	})

	return r
}
